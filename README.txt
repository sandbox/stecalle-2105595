
DESCRIPTION
-----------

Simplenews publishes and sends newsletters to lists of subscribers. This module
adds the option to simplenews for interface with a feedback node.

To do this, after the user (admins are excluse) unsubscribe itself then is
redirect to a page like

  http://www.example.com/node/1?newsl=NAME&email=EMAIL

where node/1 is the url puts in configuration and newsl and email are
respectively the newsletter name and the user email


REQUIREMENTS
------------

 * Simplenews
 * A feedback module (I have tested with a simple Webform page), optional

INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "simplenews_feedback" in the sites/all/modules
    directory and place the entire contents of this simplenews_feedback folder
    in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.

 3. ACCESS PERMISSION

    Grant the access at the Access control page:
      People > Permissions.

 4. CONFIGURE SIMPLENEWS FEEDBACK

    Configure Simplenews Feedback on the tab under Simplenews admin pages:
      Configuration > Web Services > Simplenews.

      http://www.example.com/admin/config/services/simplenews/feedback
